package com.zuitt;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;

public class Main {
    public static void main(String[] args) {
        User user1 = new User("John Smith", 25, "JSmith@mail.com", "New York");
        user1.call();

        User instructor = new User("Glenn", 33, "glenn@mail.com","Cavite");
        LocalDate localDate = LocalDate.of(2023, 7, 13);
        Date startDate = Date.from(localDate.atStartOfDay(ZoneId.systemDefault()).toInstant());
        Date endDate = Date.from(localDate.atStartOfDay(ZoneId.systemDefault()).toInstant());
        Course course1 = new Course("MACQ004", "An introduction to Java", 100, 20000.00, startDate, endDate, instructor);
        course1.call();
    }
}
